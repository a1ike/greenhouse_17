'use strict';

$(document).ready(function () {

  $('.t-goods__card-slick').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: true,
    arrows: true
  });

  $('.t-calc__card-slick').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: true,
    arrows: true
  });

  $('.t-stocks').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: true,
    arrows: true
  });

  $('.t-slider-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: false,
    arrows: true,
    asNavFor: '.t-slider-nav'
  });

  $('.t-slider-nav').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    asNavFor: '.t-slider-for',
    dots: false,
    arrows: false,
    centerMode: true,
    focusOnSelect: true,
    autoplay: true,
    responsive: [{
      breakpoint: 1199,
      settings: {
        slidesToShow: 1
      }
    }]
  });

  $('.t-papers__cards').slick({
    slidesToShow: 6,
    slidesToScroll: 1,
    dots: false,
    arrows: true,
    autoplay: false,
    centerMode: false,
    responsive: [{
      breakpoint: 1199,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    }]
  });

  $('a[href^="#"]').on('click', function (e) {
    e.preventDefault();

    var target = this.hash,
        $target = $(target);

    $('html, body').stop().animate({
      'scrollTop': $target.offset().top
    }, 900, 'swing', function () {
      window.location.hash = target;
    });
  });

  $('.phone').inputmask('+7(999)999-99-99');

  $('.call-modal').on('click', function () {
    $('.t-modal').slideToggle('fast', function (e) {
      // callback
    });
  });

  $('.t-modal__close').on('click', function () {
    $('.t-modal').slideToggle('fast', function (e) {
      // callback
    });
  });

  $('.t-header__toggler').on('click', function () {
    $('.t-header__nav').slideToggle('fast', function (e) {
      // callback
    });
  });

  /* $('#form').submit(function (e) {
    e.preventDefault();
    var f = $(this);
    $('.ierror', f).removeClass('ierror');
    var goods = $('input[name="goods"]', f).val();
    var types = $('input[name="types"]', f).val();
    var sizes = $('input[name="sizes"]', f).val();
    var amount = $('input[name="amount"]', f).val();
    var phone = $('input[name="phone"]', f).val();
    var error = false;
    if (error) {
      return false;
    }
    $.ajax({
      type: 'POST',
      url: 'mail.php',
      data: $(this).serialize(),
    }).done(function (data) {
      if (data.response == 'ok') {
        $('.c-collect__flex').slideToggle('fast', function (e) {
          // callback
        });
        $('.c-stepper__wrapper').slideToggle('fast', function (e) {
          // callback
        });
        window.location.replace('order.php');
      }
      else {
        alert('Ошибка, попробуйте повторить позже!');
      }
    });
    return false;
  }); */

  var arrType = [[13400, 14600, 17400, 20200], [11700, 12300, 13750, 15050], [11300, 11650, 12900, 13950],
  /* [12000, 12600, 14050, 15500], */
  [18150, 19500, 24100, 28700], [17500, 19700, 23400, 27200], [0, 30900, 37300, 43800]];

  var arrCount = [[3, 4, 5, 6], [3, 4, 5, 6], [3, 4, 5, 6],
  /* [3, 4, 5, 6], */
  [4, 5, 6, 7], [4, 5, 6, 7], [0, 6, 8, 10]];

  var calcType = +$('#calc-type').val();
  var calcWidth = +$('#calc-width').val();
  var calcMaterial = +$('#calc-material').val();

  $('#calc-price').text(getPrice(calcType, calcWidth, calcMaterial));

  $('#calc-type').change(function () {
    calcType = +$('#calc-type').val();
    $('#calc-price').text(getPrice(calcType, calcWidth, calcMaterial));
    $('.t-calc__card').removeClass('t-calc__card_active');
    switch (calcType) {
      case 0:
        $('#arochka-40-20').addClass('t-calc__card_active');
        $('.t-calc__card-slick').slick('setPosition');
        break;

      case 1:
        $('#arochka-20-20').addClass('t-calc__card_active');
        $('.t-calc__card-slick').slick('setPosition');
        break;

      case 2:
        $('#optima').addClass('t-calc__card_active');
        $('.t-calc__card-slick').slick('setPosition');
        break;

      case 3:
        $('#arochka-30-20').addClass('t-calc__card_active');
        $('.t-calc__card-slick').slick('setPosition');
        break;

      case 4:
        $('#fermer').addClass('t-calc__card_active');
        $('.t-calc__card-slick').slick('setPosition');
        break;

      case 5:
        $('#bogatir').addClass('t-calc__card_active');
        $('.t-calc__card-slick').slick('setPosition');
        break;
    }
  });

  $('#calc-width').change(function () {
    calcWidth = +$('#calc-width').val();
    $('#calc-price').text(getPrice(calcType, calcWidth, calcMaterial));
  });

  $('#calc-material').change(function () {
    calcMaterial = +$('#calc-material').val();
    $('#calc-price').text(getPrice(calcType, calcWidth, calcMaterial));
  });

  function getPrice(calcType, calcWitdh, calcMaterial) {
    var result = 0;
    if (arrType[calcType][calcWidth] === 0 || arrCount[calcType][calcWidth] === 0) {
      result = 'Нет в наличии';
      return result;
    }
    result = (arrType[calcType][calcWidth] + arrCount[calcType][calcWidth] * calcMaterial).toString();
    return result + ' руб.';
  }
});